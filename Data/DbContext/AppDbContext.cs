﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebApp.Data.DbModel;
using WebApp.Data.Seeder;

namespace WebApp.Data.DbContext
{
    public class AppDbContext : IdentityDbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            DataSeeder.Seed(modelBuilder);
        }
        //---------------[ Set Here ]----------------
        public DbSet<Student> Student { get; set; }
        //-------------------------------------------
    }
}
