﻿using WebApp.Data.DbContext;
using WebApp.Data.DbModel;
using WebApp.Data.Repository.Interface;

namespace WebApp.Data.Repository.Implemetation
{
    public class StudentRepository : Repository<Student>, IStudentRepository
    {
        public StudentRepository(AppDbContext context) : base(context)
        {

        }
        public List<int> GetAllRollNo()
        {
            return GetQueryable().Select(x => x.Roll).ToList();
        }
    }
}
