﻿using WebApp.Data.DbModel;

namespace WebApp.Data.Repository.Interface
{
    public interface IStudentRepository : IRepository<Student>
    {
        public List<int> GetAllRollNo();
    }
}
