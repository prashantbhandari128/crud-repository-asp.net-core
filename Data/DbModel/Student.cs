﻿using System.ComponentModel.DataAnnotations;
using WebApp.Enum;

namespace WebApp.Data.DbModel
{
    public class Student
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [StringLength(100)]
        public string? Name { get; set; }

        [Required]
        [Range(0, 50)]
        public int Roll { get; set; }

        [Required]
        public Gender Gender { get; set; }
    }
}
