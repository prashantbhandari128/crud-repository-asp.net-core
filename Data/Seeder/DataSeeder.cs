﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using WebApp.Data.DbModel;

namespace WebApp.Data.Seeder
{
    public static class DataSeeder
    {
        private static Guid adminRoleId;
        private static Guid clientRoleId;

        public static void Seed(ModelBuilder modelBuilder)
        {
            SeedRole(modelBuilder);
            SeedDefaultAdminUser(modelBuilder);
        }
        public static void SeedRole(ModelBuilder modelBuilder)
        {
            adminRoleId = Guid.NewGuid();
            clientRoleId = Guid.NewGuid();
            modelBuilder.Entity<IdentityRole>().HasData(
                new IdentityRole()
                {
                    Id = adminRoleId.ToString(),
                    Name = "Admin",
                    ConcurrencyStamp = "1",
                    NormalizedName = "ADMIN"
                },
                new IdentityRole()
                {
                    Id = clientRoleId.ToString(),
                    Name = "Client",
                    ConcurrencyStamp = "2",
                    NormalizedName = "CLIENT"
                }
            );
        }
        public static void SeedDefaultAdminUser(ModelBuilder modelBuilder)
        {
            var userId = Guid.NewGuid();
            ApplicationUser user = new ApplicationUser()
            {
                Id = userId.ToString(),
                UserName = "Admin",
                NormalizedUserName = "ADMIN",
                Email = "admin@gmail.com",
                LockoutEnabled = true,
                PhoneNumber = "9815939112",
                Address = "BTM"
            };
            PasswordHasher<ApplicationUser> passwordHasher = new PasswordHasher<ApplicationUser>();
            user.PasswordHash = passwordHasher.HashPassword(user, "Admin@123");
            modelBuilder.Entity<ApplicationUser>().HasData(user);
            modelBuilder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string>()
                {
                    UserId = userId.ToString(),
                    RoleId = adminRoleId.ToString()
                }
            );

        }
    }
}
