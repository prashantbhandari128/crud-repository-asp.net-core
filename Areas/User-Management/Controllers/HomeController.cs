﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Areas.User_Management.Controllers
{
    [Area("User-Management")]
    [Authorize]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return Content("This is Admin Area");
        }
    }
}
