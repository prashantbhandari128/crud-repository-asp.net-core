﻿using WebApp.Data.DbModel;

namespace WebApp.Service.Interface
{
    public interface IStudentService
    {
        public bool AddStudent(Student student ,IFormFile file);
        public bool UpdateStudent(Student student);
        public bool DeleteStudent(Guid id);
        public Student? GetStudentById(Guid id);
        public List<Student> GetAllStudents();
        public List<Student> GetByName(string name);
        public List<int> GetAllRollNo();
    }
}
